<?php

// SET HEADER
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: PUT");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// INCLUDING DATABASE AND MAKING OBJECT
require 'connect.php';
$db_connection = new Database();
$conn = $db_connection->dbConnection();

// $data = json_decode(file_get_contents("php://input"));
$msg = [];

if(isset($_POST['no_resi'])){

    $no_resi = $_POST['no_resi'];

    $get_resi = "SELECT * FROM resi WHERE resi.no_resi = '$no_resi'";
    $get_stmt = $conn->prepare($get_resi);
    $get_stmt->execute();
    $hasildata = $get_stmt->fetch();

    if($get_stmt->rowCount() > 0){

        $msg['message'] = 'Data Found';
        $msg['data'] = [
            'no_resi' => $no_resi,
            'user_id' => $hasildata['user_id'],
            'status_id' => $hasildata['status_id'],
            'no_ttb' => $hasildata['no_ttb'],
            'shipper' => $hasildata['shipper'],
            'receiver' => $hasildata['receiver'],
            'quantity' => $hasildata['quantity'],
            'packing' => $hasildata['packing'],
            'goods' => $hasildata['goods'],
            'kubikasi' => $hasildata['kubikasi'],
            'information' => $hasildata['information'],
            'date' => $hasildata['date'],
            'ship_arrival_date' => $hasildata['ship_arrival_date'],
            'receipt_date' => $hasildata['receipt_date']
        ];

    }else{
        $msg['message'] = 'Data Not Found';
    } 
}
else{
 $msg['message'] = 'Please fill all the fields';
}
echo  json_encode($msg);
?>