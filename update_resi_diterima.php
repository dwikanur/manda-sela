<?php
// SET HEADER
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: PUT");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// INCLUDING DATABASE AND MAKING OBJECT
require 'connect.php';
date_default_timezone_set ("Asia/Jakarta");
$db_connection = new Database();
$conn = $db_connection->dbConnection();

// GET DATA FORM REQUEST
// $data = json_decode(file_get_contents("php://input"));

if(isset($_POST['no_resi'])){

	$msg['message'] = '';

	$no_resi = $_POST['no_resi'];

	$pilih_query = "SELECT * FROM resi WHERE no_resi = '$no_resi' AND status_id = '2'";
	$pilih_stmt = $conn->prepare($pilih_query);
	$pilih_stmt->execute();

	if($pilih_stmt->rowCount() >0){

		$row = $pilih_stmt->fetch(PDO::FETCH_ASSOC);
		$receipt_date = date('Y-m-d');
		$status = $row['status_id'];
		$status = 3;

		$update_query = "UPDATE resi SET receipt_date = '$receipt_date', status_id = '$status' WHERE no_resi = '$no_resi'";
		$update_stmt = $conn->prepare($update_query);

		if($update_stmt->execute()){
			$msg['message'] = 'Data Updated Successfully';
			$msg['data'] = [
	            'no_resi' => $no_resi,
	            'status_id' =>$status,
	            'receipt_date' => $receipt_date
        ];
		}else{
			$msg['message'] = 'Data Not Updated';
		}
	}else{
		$msg['message'] = 'Data Not Found';
	}
}else{
	 $msg['message'] = 'Please fill all the fields';
}
echo  json_encode($msg);
?>