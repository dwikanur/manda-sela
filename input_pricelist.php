<?php
// SET HEADER
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: POST");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// INCLUDING DATABASE AND MAKING OBJECT
require 'connect.php';
$db_connection = new Database();
$conn = $db_connection->dbConnection();
date_default_timezone_set ("Asia/Jakarta");

// GET DATA FORM REQUEST
// $data = json_decode(file_get_contents("php://input"));

//CREATE MESSAGE ARRAY AND SET EMPTY
$msg =[];

// CHECK IF RECEIVED DATA FROM THE REQUEST
if(isset($_POST['photo'])){
    // CHECK DATA VALUE IS EMPTY OR NOT
    if(!empty($_POST['photo'])){

        $photo = $_POST['photo'];

        $insert_query = "INSERT INTO price_list (photo) VALUES(:photo)";
        $insert_stmt = $conn->prepare($insert_query);
        // DATA BINDING
        $insert_stmt->bindValue(':photo', htmlspecialchars(strip_tags($photo)),PDO::PARAM_STR);

        if($insert_stmt->execute()){
            $msg['message'] = 'Data Inserted Successfully';
            $msg['data'] = [
                'photo' => $photo,
            ];
        }
        else{
            $msg['message'] = 'Data not Inserted';
        } 
        
    }else{
        $msg['message'] = 'Oops! empty field detected. Please fill all the fields';
    }
}
else{
    $msg['message'] = 'Please fill all the fields';
}
//ECHO DATA IN JSON FORMAT
echo  json_encode($msg);
?>