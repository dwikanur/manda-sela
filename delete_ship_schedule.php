<?php
// SET HEADER
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: DELETE");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// INCLUDING DATABASE AND MAKING OBJECT
require 'connect.php';
$db_connection = new Database();
$conn = $db_connection->dbConnection();

// GET DATA FORM REQUEST
// $data = json_decode(file_get_contents("php://input"));


//CHECKING, IF ID AVAILABLE ON $data
if(isset($_POST['id'])){
    $msg['message'] = '';
    
    $id = $_POST['id'];
    
        //DELETE POST BY ID FROM DATABASE
        $delete = "DELETE FROM ship_schedule WHERE id='$id'";
        $delete_stmt = $conn->prepare($delete);
        
        if($delete_stmt->execute()){
            $msg['message'] = 'Post Deleted Successfully';
        }else{
            $msg['message'] = 'Post Not Deleted';
        }
   
    // ECHO MESSAGE IN JSON FORMAT
    echo  json_encode($msg);
}
?>