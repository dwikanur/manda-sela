<?php
// SET HEADER
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: POST");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// INCLUDING DATABASE AND MAKING OBJECT
require 'connect.php';
$db_connection = new Database();
$conn = $db_connection->dbConnection();

// GET DATA FORM REQUEST
// $data = json_decode(file_get_contents("php://input"));

//CREATE MESSAGE ARRAY AND SET EMPTY
$msg =[];

// CHECK IF RECEIVED DATA FROM THE REQUEST
if(isset($_POST['email']) && isset($_POST['name']) && isset($_POST['phone']) && isset($_POST['password'])){
    // CHECK DATA VALUE IS EMPTY OR NOT
    if(!empty($_POST['password']) && !empty($_POST['name']) && !empty($_POST['phone']) && !empty($_POST['password'])){

        //$pass = md5($data->password);

        $role_id = 2;
        $email = $_POST['email'];
        $phone = $_POST['phone'];

		$get_data="SELECT users.email, users.phone FROM users WHERE email = '$email' AND phone = '$phone'";
        $get_stmt = $conn->prepare($get_data);
        $get_stmt->execute();

        if($get_stmt->rowCount() >0){
        	$msg['message'] = 'Data is Already Inserted';
        }else{
        	$insert_query = "INSERT INTO users (role_id,email,name,phone,password) VALUES(:role_id,:email,:name,:phone,:password)";
	        $insert_stmt = $conn->prepare($insert_query);
	        	// DATA BINDING
		    $insert_stmt->bindValue(':role_id', htmlspecialchars(strip_tags($role_id)),PDO::PARAM_STR);
		    $insert_stmt->bindValue(':email', htmlspecialchars(strip_tags($email)),PDO::PARAM_STR);
		    $insert_stmt->bindValue(':name', htmlspecialchars(strip_tags($_POST['name'])),PDO::PARAM_STR);
		    $insert_stmt->bindValue(':phone', htmlspecialchars(strip_tags($phone)),PDO::PARAM_STR);
		    $insert_stmt->bindValue(':password', htmlspecialchars(strip_tags($_POST['password'])),PDO::PARAM_STR);

		    if($insert_stmt->execute()){
		    	$msg['message'] = 'Data Inserted Successfully';
	           	$msg['data'] = [
	            'email' => $email,
	            'name' => $_POST['name'],
	            'phone' => $phone
	           	];
	        }else{
	           	$msg['message'] = 'Data Not Inserted';
	        }
	    }
	}else{
		$msg['message'] = 'Oops! empty field detected. Please fill all the fields';
    }
}
else{
    $msg['message'] = 'Please fill all the fields';
}
//ECHO DATA IN JSON FORMAT
echo  json_encode($msg);
?>