<?php
// SET HEADER
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: POST");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// INCLUDING DATABASE AND MAKING OBJECT
require 'connect.php';
$db_connection = new Database();
$conn = $db_connection->dbConnection();
date_default_timezone_set ("Asia/Jakarta");

// GET DATA FORM REQUEST
// $data = json_decode(file_get_contents("php://input"));

//CREATE MESSAGE ARRAY AND SET EMPTY
$msg =[];

// CHECK IF RECEIVED DATA FROM THE REQUEST
if(isset($_POST['user_id']) && isset($_POST['no_ttb']) && isset($_POST['no_resi']) && isset($_POST['shipper']) && isset($_POST['receiver']) && isset($_POST['quantity']) && isset($_POST['packing']) && isset($_POST['goods']) && isset($_POST['kubikasi']) && isset($_POST['information'])){
    // CHECK DATA VALUE IS EMPTY OR NOT
    if(!empty($_POST['no_ttb']) && !empty($_POST['no_resi']) && !empty($_POST['shipper']) && !empty($_POST['receiver']) && !empty($_POST['quantity']) && !empty($_POST['packing']) && !empty($_POST['goods']) && !empty($_POST['kubikasi']) && !empty($_POST['information'])){

        $user_id = $_POST['user_id'];
        $no_ttb = $_POST['no_ttb'];
        $no_resi = $_POST['no_resi'];
        $shipper = $_POST['shipper'];
        $receiver = $_POST['receiver'];
        $quantity = $_POST['quantity'];
        $packing = $_POST['packing'];
        $goods = $_POST['goods'];
        $kubikasi = $_POST['kubikasi'];
        $information = $_POST['information'];

        $date = date('Y-m-d');

        $status_id = 1;

        $insert_query = "INSERT INTO resi (user_id,no_ttb,no_resi,shipper,receiver,quantity,packing,goods,kubikasi,information,date,status_id) VALUES(:user_id,:no_ttb,:no_resi,:shipper,:receiver,:quantity,:packing,:goods,:kubikasi,:information,:date,:status_id)";
        $insert_stmt = $conn->prepare($insert_query);
        // DATA BINDING
        $insert_stmt->bindValue(':user_id', htmlspecialchars(strip_tags($user_id)),PDO::PARAM_STR);
        $insert_stmt->bindValue(':no_ttb', htmlspecialchars(strip_tags($no_ttb)),PDO::PARAM_STR);
        $insert_stmt->bindValue(':no_resi', htmlspecialchars(strip_tags($no_resi)),PDO::PARAM_STR);
        $insert_stmt->bindValue(':shipper', htmlspecialchars(strip_tags($shipper)),PDO::PARAM_STR);
        $insert_stmt->bindValue(':receiver', htmlspecialchars(strip_tags($receiver)),PDO::PARAM_STR);
        $insert_stmt->bindValue(':quantity', htmlspecialchars(strip_tags($quantity)),PDO::PARAM_STR);
        $insert_stmt->bindValue(':packing', htmlspecialchars(strip_tags($packing)),PDO::PARAM_STR);
        $insert_stmt->bindValue(':goods', htmlspecialchars(strip_tags($goods)),PDO::PARAM_STR);
        $insert_stmt->bindValue(':kubikasi', htmlspecialchars(strip_tags($kubikasi)),PDO::PARAM_STR);
        $insert_stmt->bindValue(':information', htmlspecialchars(strip_tags($information)),PDO::PARAM_STR);
        $insert_stmt->bindValue(':date', htmlspecialchars(strip_tags($date)),PDO::PARAM_STR);
        $insert_stmt->bindValue(':status_id', htmlspecialchars(strip_tags($status_id)),PDO::PARAM_STR);
    
        if($insert_stmt->execute()){
            $msg['message'] = 'Data Inserted Successfully';
            $msg['data'] = [
                'user_id' => $user_id,
                'status_id' => $status_id,
                'no_ttb' => $no_ttb,
                'no_resi' => $no_resi,
                'shipper' => $shipper,
                'receiver' => $receiver,
                'quantity' => $quantity,
                'packing' => $packing,
                'goods' => $goods,
                'kubikasi' => $kubikasi,
                'information' => $information,
                'date' => $date
            ];
        }
        else{
            $msg['message'] = 'Data not Inserted';
        } 
        
    }else{
        $msg['message'] = 'Oops! empty field detected. Please fill all the fields';
    }
}
else{
    $msg['message'] = 'Please fill all the fields';
}
//ECHO DATA IN JSON FORMAT
echo  json_encode($msg);
?>